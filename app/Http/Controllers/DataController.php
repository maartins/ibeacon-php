<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\data;
use App\heatmapdata;

class DataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public  function index(Request $request)
    {
        $datas = new data();
        $items = array();

        $collection = $datas->all();
        for ($x = 1; $x < $collection->count(); $x++) {

            $tmp = [];
            $tmp['x'] = round($collection[$x]['data'][0]['x'],2);
            $tmp['y'] = round($collection[$x]['data'][0]['y'],2);
            $items[] = $tmp;

        }

        return response()->json($items);

    }



    public  function sort(Request $request)
    {
        $datas = new data();
        $items = array();

        $collection = $datas->all();
        for ($x = 0; $x <$collection->count(); $x++) {

            $tmp = [];
            $tmp['count'] = 0;
            $tmp['x'] = (int) round($collection[$x]['data'][0]['x'],0);
            $tmp['y'] = (int) round($collection[$x]['data'][0]['y'],0);


            for ($y= 0; $y < $collection->count(); $y++) {

                $x2 = round($collection[$y]['data'][0]['x'],0);
                $y2 = round($collection[$y]['data'][0]['y'],0);


                if ($tmp['x'] == $x2 && $tmp['y'] == $y2){
                    $tmp['count']++;
                }
            }

            $items[] = $tmp;


        }

        $sortArray = array_unique($items, SORT_REGULAR);
        $sortArray = array_values($sortArray);

        for ($i=0; $i < count($sortArray); $i++){
            if($sortArray[$i]['x'] && $sortArray[$i]['y']){
                print_r($sortArray[$i]['x']);
                $heatmapData = new heatmapdata();
                $heatmapData->x = $sortArray[$i]['x'];
                $heatmapData->y = $sortArray[$i]['y'];
                $heatmapData->count = $sortArray[$i]['count'];
                $heatmapData->save();
            }


        }
        ////return response()->json($items);

    }

    public function heatmap(Request $request)
    {
        $heatmapDatas = new heatmapdata();
        $items = array();

        $collection = $heatmapDatas->all();
        for ($x = 0; $x <$collection->count(); $x++) {
            $tmp = [];
            $tmp['value'] = (int) round($collection[$x]['count'],2);
            $tmp['x'] = (int) round($collection[$x]['x'],2) *-100;
            $tmp['y'] = (int) round($collection[$x]['y'],2) *100;
            $tmp['radius'] =(int) round($collection[$x]['count'],2);;
            $items[] = $tmp;
        }
        return response()->json($items);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$data = new data();
        //  $data->name = "sadfdsa";
        //$data->save();

    }
}
