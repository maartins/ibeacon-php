<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/test', 'HomeController@test');

Route::get('/data', 'DataController@index');

Route::get('/heatmap-data', 'DataController@heatmap');


Route::get('/data-sort', 'DataController@sort');

Route::post('/importFromIos', 'IosController@index');


Route::resource('api', 'ApiController');