@extends('layouts.app')

@section('content')

    <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <style type="text/css">
        .grid .tick {
            stroke: lightgrey;
            opacity: 0.7;
        }
        .grid path {
            stroke-width: 0;
        }
        .chart {
        }
        .main text {
            font: 10px sans-serif;
        }
        .axis line, .axis path {
            shape-rendering: crispEdges;
            stroke: black;
            fill: none;
        }
        circle {
            fill: steelblue;
        }
    </style>
    <script>
        var app = angular.module('chartApp', []);

        app.controller('SalesController', ['$scope','$interval','$http', function($scope, $interval, $http){

            $scope.salesData=[
                {hour: 1,sales: 54},
                {hour: 2,sales: 66},
                {hour: 3,sales: 77},
                {hour: 4,sales: 70},
                {hour: 5,sales: 60},
                {hour: 6,sales: 63},
                {hour: 7,sales: 55},
                {hour: 8,sales: 47},
                {hour: 9,sales: 55},
                {hour: 10,sales: 30}
            ];

            $interval(function(){
                var hour=$scope.salesData.length+1;
                var sales= Math.round(Math.random() * 100);
                $scope.salesData.push({hour: hour, sales:sales});
            }, 1000, 10);

            $http.get('/data').success(function(data, status, headers, config) {
                $scope.posts = data;
            }).
            error(function(data, status, headers, config) {
                // log error
            });
        }]);

        app.directive('linearChart', function($parse, $window){
            return{
                restrict:'EA',
                template:"<svg width='850' height='200'></svg>",
                link: function(scope, elem, attrs){
                    var exp = $parse(attrs.chartData);

                    var salesDataToPlot=exp(scope);
                    var padding = 20;
                    var pathClass="path";
                    var xScale, yScale, xAxisGen, yAxisGen, lineFun;

                    var d3 = $window.d3;
                    var rawSvg=elem.find('svg');
                    var svg = d3.select(rawSvg[0]);
                    var data;
                    var w = 200,
                            h = 200;
                    var margin = {
                                top: 20,
                                right: 30,
                                bottom: 60,
                                left: 20
                            }, width = 960 - margin.left - margin.right,
                            height = 500 - margin.top - margin.bottom;

                    var x = d3.scale.linear()
                            .domain([10, -10])
                            .range([0, width]);

                    var y = d3.scale.linear()
                            .domain([-10, 10])
                            .range([0, height]);

                    var chart = d3.select('.ng-scope')
                            .append('svg:svg')
                            .attr('width', width + margin.right + margin.left)
                            .attr('height', height + margin.top + margin.bottom)
                            .attr('class', 'chart');

                    var main = chart.append('g')
                            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
                            .attr('width', width)
                            .attr('height', height)
                            .attr('class', 'main');

// draw the x axis
                    var xAxis = d3.svg.axis()
                            .scale(x)
                            //.orient('bottom')
                            .orient('top'); // adjust ticks to new x axis position

                    main.append('g')
                    //.attr('transform', 'translate(0,' + height + ')')
                            .attr('transform', 'translate(0,0)') // move x axis up
                            .attr('class', 'main axis date')
                            .call(xAxis);

// draw the y axis
                    var yAxis = d3.svg.axis()
                            .scale(y)
                            .orient('left');

                    main.append('g')
                            .attr('transform', 'translate(0,0)')
                            .attr('class', 'main axis date')
                            .call(yAxis);

                    var g = main.append("svg:g");


                    scope.$watch("posts", function() {

                        data = scope.posts;
                        console.log(data);
                        g.selectAll("scatter-dots")
                                .data(data)
                                .enter().append("svg:circle")
                                .attr("cx", function (d, i) {
                                    return x(d.x);
                                })
                                .attr("cy", function (d) {
                                    return y(d.y);
                                })
                                .attr("r", 3);

                        var line = d3.svg.line()
                                .x(function(d){return x(d[0]);})
                                .y(function(d){return y(d[1]);})
                                .interpolate("linear");



                        g.append("path")
                                .attr("d", function(d) { return line(data)})
                                .attr("transform", "translate(0,0)")
                                .style("stroke-width", 2)
                                .style("stroke", "steelblue")
                                .style("fill", "none");
// end of drawing lines

                        main.append("g")
                                .attr("class", "grid")
                                .attr("transform", "translate(0," + height + ")")
                                .call(make_x_axis()
                                        .tickSize(-height, 0, 0)
                                        .tickFormat(""))

                        main.append("g")
                                .attr("class", "grid")
                                .call(make_y_axis()
                                        .tickSize(-width, 0, 0)
                                        .tickFormat(""))

                        function make_x_axis() {
                            return d3.svg.axis()
                                    .scale(x)
                                    .orient("bottom")
                                    .ticks(10)
                        }

                        function make_y_axis() {
                            return d3.svg.axis()
                                    .scale(y)
                                    .orient("left")
                                    .ticks(10)
                        }

                    });

// begin of drawing lines

                }
            };
        });
    </script>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <div ng-app="chartApp" ng-controller="SalesController as ctrl">
                        <h1>Today's Sales Chart</h1>
                        <div linear-chart chart-data="salesData"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
