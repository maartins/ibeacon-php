@extends('layouts.app')

@section('content')
    <style>
        .map {
            margin: 0;
            padding: 0;
            height: 100%;
        }

        body {
            font-family: sans-serif;
        }

        #heatmapContainerWrapper {
            width: 100%;
            height: 100%;
            position: absolute;
        }

        #heatmapContainer {
            width: 100%;
            height: 100%;
        }

        h1 {
            position: absolute;
            background: black;
            color: white;
            padding: 10px;
            font-weight: 200;
        }

        #all-examples-info {
            position: absolute;
            background: white;
            font-size: 16px;
            padding: 20px;
            top: 100px;
            width: 350px;
            line-height: 150%;
            border: 1px solid rgba(0, 0, 0, .2);
        }

        heatmap {
            width: 100%;
            height: 100%;
            position: absolute;
        }
    </style>

    <script  src=" {{ URL::asset('js/heatmap.js') }}"></script>
    <script  src=" {{ URL::asset('js/angular-heatmap.js') }}"></script>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="map" ng-app="heatmapApp">
                        <div ng-controller="HeatmapCtrl">
                            <heatmap id="heatmap-1" data="heatmapData" config="heatmapConfig"
                                     ng-click="updateData()"></heatmap>
                        </div>
                        <script>
                            angular.module('heatmapApp', ['heatmap'])
                                    .controller('HeatmapCtrl', ['$scope', '$heatmap','$http', function ($scope, $heatmap, $http) {
                                        function generateRandomData(len) {
                                            var max = 20;
                                            var min = 1;
                                            var maxX = document.body.clientWidth;
                                            var maxY = document.body.clientHeight;
                                            var data = [];

                                            $http.get('/heatmap-data').success(function(data, status, headers, config) {
                                                $scope.response = data;
                                            });
                                            return {
                                                max: max,
                                                min: min,
                                                data: $scope.response
                                            }
                                        };
                                        $scope.heatmapData = generateRandomData();
                                        $scope.heatmapConfig = {
                                            blur: .9,
                                            opacity: .5
                                        };

                                        $scope.updateData = function () {
                                            $scope.heatmapData = generateRandomData(1000);
                                        };
                                    }])
                                    .run();
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
